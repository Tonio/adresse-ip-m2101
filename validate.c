#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "validate.h"

//valide que la partie observée est un nombre (valide si c'est un nombre positif)
int validate_number(char *str) {
   while (*str) {
      if(!isdigit(*str)){//si le charactère observé n'est un nombre retourne faux
         return 0;
      }
      str++; //passe au caractère suivant.
   }
   return 1;//si tous les caractères sont des nombres retourne vrai.
}

//valide l'adresse ip renseignée en utilisant le séparateur "."
//l'ip est est séparé du masque en amont par la fonction separation_format
int validate_ip(char *ip,int *ptid,size_t size) {
   int num, dots = 0;
   char *ptr;
   size_t i = 0;
   if (ip == NULL){ //verifie si une ip est bien renseignée
      return 0;
   }
   ptr = strtok(ip, "."); // si oui la sépare avec avec le séparateur "."
   if (ptr == NULL){ //si ce qu'il y a entre le séparateur est vide retourne 0
      return 0;
   }
   while (ptr) {//sinon vérifie que c'est composé d'une valeur numérique
      if (!validate_number(ptr)){
         return 0;
      }
      num = atoi(ptr); //numérise la valeur et vérifie si elle est dans les valeurs prévus pour l'ip
      if (num >= 0 && num <= 255) {
         //mise dans un tableau d'int des différentes parties
            ptid[i++]=num; //si oui passe la valeur dans un tableau d'entiers et valide cette partie en rajoutant 1 à la variable dots
            ptr = strtok(NULL, ".");
            if (ptr != NULL){
               dots++;
            }
         } 
         else if(num>255){//sinon le rajoute quand même dans le tableau sans incrémenter dots pour dire que l'ip est invalide.
            ptid[i++]=num;
            ptr = strtok(NULL, ".");
            }
         else{
            return 0;
         }
    }
    if (dots != 3){//si il n'y a pas 3 points dans l'ip alors cela retourne que l'ip est invalide
       return 0;
    }
      return 1;
}

//valide le masque renseigné
int validate_mask(char *mask){
   int num;
   if(mask==NULL){ //vérifie qu'un masque est renseigné
      return 0;
   }
   if (!validate_number(mask)){ //si ce n'est pas un nombre
      return 0;
   }
   else {
      num = atoi(mask); //sinon le numérise
   }

   if(num >= 1 && num <= 32){// et vérifie si y il est valide
      return 1;
   }
   return 0;
}

//valide le format complet de l'adresse renseignée en entrée
//si l'adresse ip et le masque son valide renvoit 1
//si l'adresse ip est renseignée seule sans le masque et qu'elle est valide renvoit 2
//sinon renvoit 0 dans tous les autres cas
int validation_format(char **ip,char **mask,int * slash,int *ptid){
  if(validate_ip(*ip,ptid,4)==1){
     if(*slash==1){
        if(validate_mask(*mask)==1){ //ip et masque valide
               return 1;
        }
        else{ //ip valide mais masque invalide
               return 0;
        }
     }
     else if(*slash==0){
               return 2; //ip valide mais pas de masque
     }
     else{
               return 0;
     }
  }
  else{ //ip invalide
               return 0;
  }
}

//sépare la partie ip et la partie masque avec le séparateur "/"
void separation_format(char *ch,char **ip,char **mask,int * slash){

    char* p;
    p = strtok(ch, "/");//separe le masque de l'ip
    *ip = p;//récupère la partie ip
    while (p != NULL)

    {
        *mask = p;//récupère la partie masque
        p = strtok(NULL, "/");
        (*slash)++;//incrémente en fonction du nombre de slash présent
    }
}