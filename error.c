#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "validate.h"

//creation d'un fichier qui renvoit les erreurs pour un format invalide
void creation_fichier_erreur(int *ipv4,char **mask, int *slash){ 
   FILE* fic;

   fic = fopen("ip.txt", "w");
   if (fic == NULL) {
      printf("Erreur à l'ouverture du fichier texte\n");
   } else {
     fprintf(fic,"Erreur dans l'adresse renseigné: \n");
     for (int i=0;i<4;i++){
        if(ipv4[i]>255){
           fprintf(fic,"Partie %d de l'IP supérieur à 255\n",i+1);
        }
        else if(ipv4[i]==-1){
           fprintf(fic,"Partie %d de l'IP manquante. Vérifier que votre ip soit au format suivant: X.X.X.X/D ou X.X.X.X\n",i+1);
        }
     }

      fclose(fic);
   }
}
