#include <stdio.h>
#include <stdlib.h>
#include "fichier.h"
//créer le fichier résultat ip.txt
void creation_fichier(int *ipv4,int masque,int *hote,int *s_reseau,int *reseau,int *p_rest,char *classe_ip,char *type_ip){
   FILE* fic;

   fic = fopen("ip.txt", "w");
   if (fic == NULL) {
      printf("Erreur à l'ouverture du fichier texte\n");
   } else {
      fprintf(fic, "\nAdresse renseigné: %d.%d.%d.%d/%d\n\n",ipv4[0] ,ipv4[1] ,ipv4[2] ,ipv4[3] ,masque);
      fprintf(fic,"Etude de l'IP: %d.%d.%d.%d\n",ipv4[0] ,ipv4[1] ,ipv4[2] ,ipv4[3]);
      fprintf(fic, "Classe: %s\n", classe_ip);
      fprintf(fic, "Type: %s\n\n", type_ip);
      fprintf(fic, "Etude du masque:\n");
      if(masque!=0){
         fprintf(fic, "Format CIDR: /%d\n",masque);
         fprintf(fic, "Adresse Masque: %d.%d.%d.%d\n", reseau[0] ,reseau[1] ,reseau[2] ,reseau[3]);
         fprintf(fic, "Adresse Reseau: %d.%d.%d.%d\n", s_reseau[0] ,s_reseau[1] ,s_reseau[2] ,s_reseau[3]);
         fprintf(fic, "Adresse Hote: ");
         for(int i=0;i<4;i++){
            if(hote[i]==0){
               fprintf(fic,"X");
            }
            else
            {
               fprintf(fic,"%d",hote[i]);
            }
            if (i!=3)
            {
               fprintf(fic,".");
            }
         }
         fprintf(fic, "\n\n");
      }
      else{
         fprintf(fic, "Adresse Reseau: ERROR NO MASK\n");
         fprintf(fic, "Adresse Hote: ERROR NO MASK\n");
      }

      fclose(fic);
   }
}
//affiche le fichier résultat ip.txt
void afficher_fichier(){
   FILE* fic;
   fic = fopen("ip.txt", "r");
   if (fic != NULL) {
      char* ligne = NULL;
      size_t taille = 0;
      getline(&ligne, &taille, fic);
      while (!feof(fic)) {
         printf("%s", ligne);
         getline(&ligne, &taille, fic);
      }
   free(ligne);
   fclose(fic);
   }
}